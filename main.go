package main

import (
	"hadePro/framework"
	"net/http"
)

func main() {
	server := &http.Server{
		Addr:    ":8080",
		Handler: framework.NewCore(),
	}
	err := server.ListenAndServe()
	if err != nil {
		return
	}
}
