package demo

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestCtx(t *testing.T) {
	const delay = 1000 * time.Millisecond
	d := time.Now().Add(delay)
	ctx, cancel := context.WithDeadline(context.Background(), d)
	defer cancel()
	select {
	case <-time.After(1 * time.Second):
		fmt.Println("second")
	case <-ctx.Done():
		fmt.Println("overslept")
	}
}
